module gitlab.com/eemj/ssh-grafana-geoip

go 1.14

require (
	github.com/coreos/go-systemd/v22 v22.1.0
	github.com/influxdata/influxdb1-client v0.0.0-20200515024757-02f0bf5dbca3
	github.com/mmcloughlin/geohash v0.10.0
	github.com/oschwald/geoip2-golang v1.4.0
)
