package main

import (
	"io"
	"net"
	"regexp"
	"time"

	"github.com/influxdata/influxdb1-client"
	"github.com/mmcloughlin/geohash"
	"github.com/oschwald/geoip2-golang"
)

const (
	timeLayout = "2006-01-02 15:04:05.000000 -0700 MST"
	timeFormat = "2006-01-02 15:04:05.000"
)

var (
	// Disconnected from invalid user yuxin 222.105.157.3 port 47972 [preauth]
	disconnectInvalidUser = regexp.MustCompile(`Disconnected from invalid user (\w+) (\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) port (\d{1,5}) \[preauth\]`)

	// Invalid user oracle from 113.105.121.202 port 12210
	invalidUser = regexp.MustCompile(`Invalid user (\w+) from (\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) port (\d{1,5})`)

	// User root from 123.13.221.191 not allowed because not listed in AllowUsers
	notInAllowUsers = regexp.MustCompile(`User (\w+) from (\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) not allowed because not listed in AllowUsers`)
)

type parser struct {
	io.Writer
	c  *client.Client
	db *geoip2.Reader
	m  string // measurement
	d  string // database
}

func (p *parser) Write(c []byte) (n int, err error) {
	n = len(c)
	s := string(c)
	timestamp := s[:len(timeLayout)]
	t, _ := time.Parse(timeLayout, timestamp)

	var (
		point   *client.Point
		matches []string
	)

	switch { // fortunately they're all in the same order
	case disconnectInvalidUser.MatchString(s):
		matches = disconnectInvalidUser.FindStringSubmatch(s)
	case invalidUser.MatchString(s):
		matches = invalidUser.FindStringSubmatch(s)
	}

	if len(matches) > 0 {
		address := net.ParseIP(matches[2])
		country, _ := p.db.Country(address)
		city, _ := p.db.City(address)

		port := ""

		if len(matches) > 2 {
			port = matches[3]
		}

		point = &client.Point{
			Fields: map[string]interface{}{"count": 1},
			Tags: map[string]string{
				"timestamp": t.Format(timeFormat),
				"geohash": geohash.Encode(
					city.Location.Latitude, city.Location.Longitude,
				),
				"host":         address.String(),
				"port":         port,
				"country_code": country.Country.IsoCode,
				"username":     matches[1],
			},
			Measurement: p.m,
		}
	}

	if point != nil {
		_, err = p.c.Write(client.BatchPoints{
			Points:   []client.Point{*point},
			Database: p.d,
		})
	}

	return
}
