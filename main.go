package main

import (
	"errors"
	"flag"
	"log"
	"net/url"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/influxdata/influxdb1-client"
	"github.com/coreos/go-systemd/v22/sdjournal"
	"github.com/coreos/go-systemd/v22/util"
	"github.com/oschwald/geoip2-golang"
)

var (
	ErrSystemDNotRunning = errors.New("systemd is not running")
)

func main() {
	if !util.IsRunningSystemd() {
		log.Fatal(ErrSystemDNotRunning)
	}

	var username,
		password,
		database,
		host,
		measurement,
		geoipPath,
		systemdService string

	flag.StringVar(&username, "u", "", "influxdb username")
	flag.StringVar(&password, "p", "", "influxdb password")
	flag.StringVar(&database, "c", "", "influxdb database name")
	flag.StringVar(&measurement, "m", "", "influxdb measurement")
	flag.StringVar(&host, "addr", "", "influxdb host (e.g: http://127.0.0.1:8086..)")
	flag.StringVar(&geoipPath, "geoip", "", "geoip2 path")
	flag.StringVar(&systemdService, "s", "sshd.service",
		"systemd service (e.g: sshd.service, ssh.service...)")

	flag.Parse()

	reader, err := sdjournal.NewJournalReader(sdjournal.JournalReaderConfig{
		Matches: []sdjournal.Match{{
			Value: systemdService,
			Field: sdjournal.SD_JOURNAL_FIELD_SYSTEMD_UNIT,
		}},
		NumFromTail: 0,
	})

	if err != nil {
		log.Fatal(err)
	}

	defer reader.Close()

	db, err := geoip2.Open(geoipPath)

	if err != nil {
		log.Fatal(err)
	}

	defer db.Close()

	url, err := url.Parse(host)

	if err != nil {
		log.Fatal(err)
	}

	c, err := client.NewClient(client.Config{
		Username: username,
		Password: password,
		Timeout:  client.DefaultTimeout,
		URL:      *url,
	})

	if err != nil {
		log.Fatal(err)
	}

	p := &parser{
		c: c, db: db, m: measurement, d: database,
	}

	stop := make(chan time.Time)
	s := make(chan os.Signal)
	signal.Notify(s, syscall.SIGINT, syscall.SIGTERM)

	go reader.Follow(stop, p)

	<-s
	close(stop)
}
